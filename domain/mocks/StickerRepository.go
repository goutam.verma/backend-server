package mocks

import (
	context "context"

	domain "github.com/GoutamVerma/backend-server/domain"
	mock "github.com/stretchr/testify/mock"
)

type StickerRepository struct {
	mock.Mock
}

func (_m *StickerRepository) DeleteStickers(ctx context.Context) error {
	ret := _m.Called(ctx)
	return ret.Error(0)
}

func (_m *StickerRepository) GetStickers(ctx context.Context) ([]domain.Stickers, error) {
	ret := _m.Called(ctx)
	return ret.Get(0).([]domain.Stickers), ret.Error(1)
}

func (_m *StickerRepository) AddSticker(_a0 context.Context, _a1 *domain.Stickers) error {
	ret := _m.Called(_a0, _a1)
	return ret.Error(0)
}

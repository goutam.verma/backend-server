package domain

import (
	"context"
	"time"
)

type Stickers struct {
	ID       int64     `json:"id"`
	Name     string    `json:"name" validate:"required"`
	Priority int       `json:"priority" validate:"required"`
	AddedAt  time.Time `json:"added_at"`
}

type StickerUsecase interface {
	AddSticker(context.Context, *Stickers) error
	DeleteStickers(ctx context.Context) error
	GetTrendingStickers(ctx context.Context) ([]Stickers, error)
}

type StickerRepository interface {
	GetStickers(ctx context.Context) ([]Stickers, error)
	AddSticker(ctx context.Context, a *Stickers) error
	DeleteStickers(ctx context.Context) error
}

.PHONY: all build run test clean

all: build

build:
	go build -o backend-server app/main.go

run:
	./backend-server

test:
	go test -v ./...

clean:
	rm -f backend-server

docker-build:
	docker build -t backend-server .
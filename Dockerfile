FROM golang:latest

WORKDIR /app

# Copy the Go application source code into the container
COPY . .

# Set environment variables
ENV returnLimit: ${RETURNLIMIT}
ENV username : "${USERNAME}"
ENV password : "${PASSWORD}"
ENV hostname : "${HOSTNAME}"
ENV port : ${PORT}
ENV databaseName : "${DATABASENAME}"
ENV pageSize : ${PAGE_SIZE}



EXPOSE 8080

CMD ["go", "run", "app/main.go"]

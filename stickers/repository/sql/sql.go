package sql

import (
	"context"
	"log"

	domain "github.com/GoutamVerma/backend-server/domain"
	"gorm.io/gorm"
)

type mysqlStickerRepository struct {
	Conn *gorm.DB
}

func NewSQLStickerRepository(conn *gorm.DB) domain.StickerRepository {
	conn.AutoMigrate(&domain.Stickers{})
	return &mysqlStickerRepository{conn}
}

func (repo *mysqlStickerRepository) AddSticker(ctx context.Context, model *domain.Stickers) error {
	err := repo.Conn.Create(model).Error
	if err != nil {
		return err
	}
	log.Println("Record inserted successfully")
	return nil
}

func (repo *mysqlStickerRepository) GetStickers(ctx context.Context) ([]domain.Stickers, error) {
	var stickers []domain.Stickers
	err := repo.Conn.Find(&stickers).Error
	if err != nil {
		return nil, err
	}
	return stickers, nil
}

func (repo *mysqlStickerRepository) DeleteStickers(ctx context.Context) error {
	err := repo.Conn.Exec("DELETE FROM stickers").Error
	if err != nil {
		return err
	}
	log.Println("Stickers table content deleted successfully")
	return nil
}

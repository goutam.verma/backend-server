package sql_test

import (
	"context"
	"testing"
	"time"

	"github.com/GoutamVerma/backend-server/domain"
	"github.com/GoutamVerma/backend-server/stickers/repository/sql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func setupTestDB(t *testing.T) *gorm.DB {
	db, err := gorm.Open(sqlite.Open(":memory:"), &gorm.Config{})
	if err != nil {
		t.Fatalf("failed to connect to test database: %v", err)
	}

	err = db.AutoMigrate(&domain.Stickers{})
	if err != nil {
		t.Fatalf("failed to migrate tables: %v", err)
	}

	return db
}

func closeTestDB(db *gorm.DB) {
	sqlDB, err := db.DB()
	if err != nil {
		panic(err)
		return
	}
	sqlDB.Close()
}

func TestAddSticker(t *testing.T) {
	db := setupTestDB(t)
	defer closeTestDB(db)

	repo := sql.NewSQLStickerRepository(db)

	sticker := &domain.Stickers{
		Name:     "Test Sticker",
		Priority: 1,
	}

	err := repo.AddSticker(context.Background(), sticker)
	if err != nil {
		t.Errorf("error adding sticker: %v", err)
	}

	var count int64
	db.Model(&domain.Stickers{}).Count(&count)
	if count != 1 {
		t.Errorf("expected 1 sticker, got %d", count)
	}
}

func TestGetStickers(t *testing.T) {
	db := setupTestDB(t)
	defer closeTestDB(db)

	repo := sql.NewSQLStickerRepository(db)
	addedTime := time.Now()
	stickers := []domain.Stickers{
		{ID: 1, Name: "Sticker 1", Priority: 0, AddedAt: addedTime},
		{ID: 2, Name: "Sticker 2", Priority: 5, AddedAt: addedTime},
	}

	for _, sticker := range stickers {
		err := repo.AddSticker(context.Background(), &sticker)
		if err != nil {
			t.Fatalf("error adding sticker: %v", err)
		}
	}

	resultStickers, err := repo.GetStickers(context.Background())
	if err != nil {
		t.Errorf("error getting stickers: %v", err)
	}

	if len(resultStickers) != len(stickers) {
		t.Errorf("expected %d stickers, got %d", len(stickers), len(resultStickers))
	}
}

func TestDeleteStickers(t *testing.T) {
	db := setupTestDB(t)
	defer closeTestDB(db)

	repo := sql.NewSQLStickerRepository(db)

	stickers := []domain.Stickers{
		{Name: "Sticker 1", Priority: 1},
		{Name: "Sticker 2", Priority: 2},
	}

	for _, sticker := range stickers {
		err := repo.AddSticker(context.Background(), &sticker)
		if err != nil {
			t.Fatalf("error adding sticker: %v", err)
		}
	}

	err := repo.DeleteStickers(context.Background())
	if err != nil {
		t.Errorf("error deleting stickers: %v", err)
	}

	var count int64
	db.Model(&domain.Stickers{}).Count(&count)
	if count != 0 {
		t.Errorf("expected 0 stickers after deletion, got %d", count)
	}
}

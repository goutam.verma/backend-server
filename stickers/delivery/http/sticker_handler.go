package http

import (
	"net/http"
	"time"

	domain "github.com/GoutamVerma/backend-server/domain"
	errors "github.com/GoutamVerma/backend-server/domain"
	"github.com/sirupsen/logrus"

	"github.com/labstack/echo/v4"
)

type StickerHandler struct {
	SUsecase domain.StickerUsecase
}

func NewStickerHandler(e *echo.Echo, us domain.StickerUsecase) {
	handler := &StickerHandler{
		SUsecase: us,
	}

	e.GET("/v1/trendingStickers", handler.GetStickers)
	e.POST("/v1/trendingStickers", handler.AddSticker)
	e.GET("/v1/deleteStickers", handler.DeleteAllStickers)
}

func (s *StickerHandler) AddSticker(c echo.Context) error {
	var data domain.Stickers

	if err := c.Bind(&data); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, errors.ResponseError{Message: err.Error()})
	}
	data.AddedAt = time.Now()

	ctx := c.Request().Context()
	err := s.SUsecase.AddSticker(ctx, &data)
	if err != nil {
		return c.JSON(GetStatusCode(err), errors.ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, data)
}

func (s *StickerHandler) GetStickers(c echo.Context) error {
	ctx := c.Request().Context()

	stkr, err := s.SUsecase.GetTrendingStickers(ctx)
	if err != nil {
		return c.JSON(GetStatusCode(err), errors.ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, stkr)
}

func (s *StickerHandler) DeleteAllStickers(c echo.Context) error {
	ctx := c.Request().Context()
	err := s.SUsecase.DeleteStickers(ctx)
	if err != nil {
		return c.JSON(GetStatusCode(err), errors.ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, errors.ResponseError{Message: "Stickers deleted successfully"})
}

func GetStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	logrus.Error(err)
	switch err {
	case errors.ErrInternalServerError:
		return http.StatusInternalServerError
	case errors.ErrNotFound:
		return http.StatusNotFound
	case errors.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}

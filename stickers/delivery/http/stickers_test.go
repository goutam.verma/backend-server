package http_test

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/GoutamVerma/backend-server/domain"
	"github.com/GoutamVerma/backend-server/domain/mocks"
	https "github.com/GoutamVerma/backend-server/stickers/delivery/http"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestAddStickerHandler(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/v1/trendingStickers", strings.NewReader(`{"ID":1,"Name":"Sticker1","Priority":1}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	mockStickerUsecase := &mocks.StickerUsecase{}
	mockStickerUsecase.On("AddSticker", mock.Anything, mock.Anything).Return(nil)

	handler := &https.StickerHandler{
		SUsecase: mockStickerUsecase,
	}

	if assert.NoError(t, handler.AddSticker(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Contains(t, rec.Body.String(), "Sticker1")
	}
}

func TestGetStickersHandler(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/v1/trendingStickers", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	mockStickerUsecase := &mocks.StickerUsecase{}
	mockStickerUsecase.On("GetTrendingStickers", mock.Anything).Return([]domain.Stickers{
		{
			ID:       1,
			Name:     "Sticker1",
			Priority: 1,
			AddedAt:  time.Now(),
		},
	}, nil)

	handler := &https.StickerHandler{
		SUsecase: mockStickerUsecase,
	}

	if assert.NoError(t, handler.GetStickers(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Contains(t, rec.Body.String(), "Sticker1")
	}
}

func TestDeleteAllStickersHandler(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/v1/deleteStickers", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	mockStickerUsecase := &mocks.StickerUsecase{}
	mockStickerUsecase.On("DeleteStickers", mock.Anything).Return(nil)

	handler := &https.StickerHandler{
		SUsecase: mockStickerUsecase,
	}

	if assert.NoError(t, handler.DeleteAllStickers(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Contains(t, rec.Body.String(), "Stickers deleted successfully")
	}
}

package usecase_test

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/GoutamVerma/backend-server/domain"
	"github.com/GoutamVerma/backend-server/stickers/usecase"
	"github.com/spf13/viper"
)

// mockStickerRepository is a mock implementation of domain.StickerRepository for testing purposes.
type mockStickerRepository struct{}

func (m *mockStickerRepository) GetStickers(ctx context.Context) ([]domain.Stickers, error) {
	// Round the current time to the nearest second
	now := time.Now().Round(time.Second)

	stickers := []domain.Stickers{
		{Name: "Sticker 1", Priority: 1, AddedAt: now},
		{Name: "Sticker 2", Priority: 2, AddedAt: now.Add(-48 * time.Hour)},
		{Name: "Sticker 3", Priority: 3, AddedAt: now.Add(-12 * time.Hour)},
	}
	return stickers, nil
}

func (m *mockStickerRepository) AddSticker(ctx context.Context, s *domain.Stickers) error {
	return nil
}

func (m *mockStickerRepository) DeleteStickers(ctx context.Context) error {
	return nil
}

func TestStickerUsecase_GetTrendingStickers(t *testing.T) {
	viper.Set("returnLimit", 2) // Set returnLimit for testing
	defer viper.Reset()

	usecase := usecase.NewStickerUsecase(&mockStickerRepository{})
	ctx := context.Background()

	expected := []domain.Stickers{
		{Name: "Sticker 3", Priority: 3, AddedAt: time.Now().Round(time.Second).Add(-12 * time.Hour)},
		{Name: "Sticker 1", Priority: 1, AddedAt: time.Now().Round(time.Second)},
	}

	trendingStickers, err := usecase.GetTrendingStickers(ctx)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	if !reflect.DeepEqual(trendingStickers, expected) {
		t.Errorf("Expected trending stickers: %v, but got: %v", expected, trendingStickers)
	}
}
func TestStickerUsecase_AddSticker(t *testing.T) {
	usecase := usecase.NewStickerUsecase(&mockStickerRepository{})
	ctx := context.Background()

	sticker := &domain.Stickers{Name: "New Sticker", Priority: 4}

	err := usecase.AddSticker(ctx, sticker)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
}

func TestStickerUsecase_DeleteStickers(t *testing.T) {
	usecase := usecase.NewStickerUsecase(&mockStickerRepository{})
	ctx := context.Background()

	err := usecase.DeleteStickers(ctx)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
}

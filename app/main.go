package main

import (
	"fmt"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	_stickerHttpDelivery "github.com/GoutamVerma/backend-server/stickers/delivery/http"
	_stickerRepo "github.com/GoutamVerma/backend-server/stickers/repository/sql"
	_stickerUse "github.com/GoutamVerma/backend-server/stickers/usecase"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
	if viper.GetBool("debug") {
		log.Println("Service running in DEBUG mode")
	}
}

func main() {
	username := viper.GetString("username")
	password := viper.GetString("password")
	hostname := viper.GetString("hostname")
	port := viper.GetInt("port")
	databaseName := viper.GetString("databaseName")

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", username, password, hostname, port, databaseName)
	log.Printf(dsn)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	ar := _stickerRepo.NewSQLStickerRepository(db)
	au := _stickerUse.NewStickerUsecase(ar)
	_stickerHttpDelivery.NewStickerHandler(e, au)

	e.Logger.Fatal(e.Start(":8080"))
}

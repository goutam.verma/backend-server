# Backend-Server (BobbleAI)

Created a simple Echo project which has an endpoint called `/v1/trendingStickers` which fetches a list of trending stickers from the database based on the priority and current time of day. Run migrations and populate the database with appropriate values. Make the number of stickers returned configurable. Ensure that test cases are added.


## How it works
- When a client requests data from Echo, using our REST API, we read the DB for latest data and send back a paginated JSON response.

- When a client post data to endpoint, using the POST request, we store the data into the SQL database.

- The response received is stored in the DB(SQL).

## Endpoints
  1. **Get trending stickers**
     - Endpoint: `/v1/trendingStickers`
     - Method: `GET`
     - Parameters: `None`
     - Example: `curl -X GET "http://localhost:8080/v1/trendingStickers"`
     - Response: 
       ```json
       [{"ID":34,"Name":"GalaxySticker","Priority":10,"AddedAt":"2024-01-28T18:39:17Z"},
       {"ID":94,"Name":"NobleSticker","Priority":10,"AddedAt":"2024-01-28T18:39:26Z"},
       {"ID":93,"Name":"MystiqueSticker","Priority":10,"AddedAt":"2024-01-28T18:39:26Z"},
       {"ID":4"Name":"ZenSticker""Priority":10,"AddedAt":"2024-01-28T18:39:13Z"}]
       ```

  2. **Add Stickers**
     - Endpoint: `/v1/trendingStickers`
     - Method: `POST`
     - Parameters: `ID`, `Name`,`Priority`
     - Example: `curl -X POST -d "
     data = {
        "ID" : 1,
        "Name": "Car sticker",
        "Priority": 0
        }" 
    "http://localhost:8080/v1/trendingStickers`
     - Response:
       ```json
       "200"
       ```
  3. **Delete all stickers**
     - Endpoint: `/v1/deleteStickers`
     - Method: `GET`
     - Parameters: `None`
     - Example: `curl -X GET "http://localhost:8080/v1/deleteStickers`
     - Response:
       ```json
       "Deleted"
       ```

## How to run this project

### Using Source Code (Recommended)
1. Clone the Gitlab repository:
    ```
    $ https://gitlab.com/goutam.verma/backend-server.git
    $ cd backend-server
    ```
2. Before starting the server, provide the necessary information in the backend-server.yaml file. This includes the credentials for the database (AWS RDS) and YouTube API keys.

    Example: 
    ```
    rate_limit: 50
    username : "abc"
    password : "xyz123"
    hostname : "stickers.c165416548svs.ap-south-1.rds.portalaws.com"
    port : 3306 
    databaseName : "trendingStickers"
    ```
    This file contains the desired information required for the application.

3. Once you have provided the desired information, you can start the server.Run the following command to start the Echo server:

    ```
    $ make
    $ make run
    ```
    Or
    ```
    $ go run app/main.go
    ```

### Using the Dockerfile

To use the Dockerfile in this project, follow these steps:

1. Make sure you have the Docker installed on your system.

2. Open the `backend-server.yaml` file located in the root directory of the project. Provide the necessary information, such as the credits for AWS RDS, in the `backend-server.yaml` file or you can provide it via docker run env command.

3. Once you have provided the required information, you can build the Docker image using the Dockerfile. Run the following command in the terminal:

    ```bash
    docker build -t backend-server:latest .

    or 
    make docker-build
    ```

    This command will build the Docker image with the tag `backend-server:latest`.

4. After the Docker image is built, you can run it using the following command: 
    example:

    ```bash
    docker run -it \
    -e RETURNLIMIT="XX" \
    -e USERNAME="username" \
    -e PASSWORD="abc123" \
    -e HOSTNAME="database.c541sgfg484adads.ap-north-1.rds.platformaws.com" \
    -e PORT="3306" \
    -e DATABASENAME="trendingStickers" \
    -e PAGE_SIZE="3" \
    -p 8080:8080 \
    backend-server:latest
    ```

    This command will run the Docker container and map port 8080 of the container to port 8080 of the host machine.

Now you should be able to access your application running inside the Docker container at `http://localhost:8080`.